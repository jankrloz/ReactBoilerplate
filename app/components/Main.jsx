import React from "react";

var Main = (props) => {
    return (
        <div>
            <h1>Welcome</h1>
            {props.children}    
        </div>
    )
};

module.exports = Main;
